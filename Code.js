function searchTerms() {
  var d = new Date();
  d.setDate(d.getDate() - 2);
  var u = new Date();
  var start = getFormattedDate(d);
  var end = getFormattedDate(u);

  return "after:" + start + " before:" + end;
}

function getFormattedDate(date) {
  var year = date.getFullYear();

  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : "0" + month;

  var day = date.getDate().toString();
  day = day.length > 1 ? day : "0" + day;

  return year + "/" + month + "/" + day;
}

function doGet(request) {
  var r = getRecentMsgs();
  var result = JSON.stringify(r);

  return ContentService.createTextOutput(JSON.stringify(result)).setMimeType(
    ContentService.MimeType.JSON
  );
}

function getRecentMsgs() {
  var msgArray = [];
  var searchT = searchTerms();
  var threads = GmailApp.search(searchT);
  var msgNum = 0;
  for (var j = 0; j < threads.length; j++) {
    var messages = threads[j].getMessages();
    for (var t = 0; t < messages.length; t++) {
      if (messages[t].isInInbox() == true) {
        msgNum++;
        var out = {
          id: messages[t].getId(),
          //"subject": messages[t].getSubject(),
          //"from": messages[t].getFrom(),
          //"to": messages[t].getTo().concat(' ', messages[t].getCc()),
          body: messages[t].getPlainBody(),
          date: messages[t].getDate(),
        };

        msgArray.push(out);
      }
    }
  }
  return { messages: msgArray, noOfMsgs: msgNum };
}

function getOneLabel(labelName) {
  var msgArray = [];
  var msgNum = 0;
  var labelObject = GmailApp.getUserLabelByName(labelName);
  var name = labelName;
  var offset = getOffset(name);
  var threads = labelObject.getThreads();
  for (var j = 0; j < threads.length; j++) {
    var messages = threads[j].getMessages();
    for (var t = 0; t < messages.length; t++) {
      msgNum++;
      var out = {
        name: name,
        subject: messages[t].getSubject(),
        from: messages[t].getFrom(),
        to: messages[t].getTo().concat(" ", messages[t].getCc()),
        body: messages[t].getPlainBody(),
        date: messages[t].getDate(),
      };

      msgArray.push(out);
    }
  }
  return { messages: msgArray, noOfMsgs: msgNum };
}
